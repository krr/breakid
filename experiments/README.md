Experiments for BreakID 2.1
========
The folder /raw contains raw experimental data. 

The file /benchmark_set.ods is a spreadsheet in the open document format, readable by most office suites. It summarizes information on the symmetry properties of each instance, including the number of symmetry generators detected, the number of binary symmetry breaking clauses constructed, the number of row-interchangeability matrices detected etc. 

The file /timings.ods is a spreadsheet in the open document format, readable by most office suites. It summarizes timeouts of each instance for different solving configurations, combining Glucose with Shatter and BreakID. These measurements include any preprocessing time needed, e.g. to detect symmetry or remove duplicate clauses from the CNF.